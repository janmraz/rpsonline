import React from 'react'

class choosing extends React.Component {
    constructor(){
        super();
        this.handleChange = this.handleChange.bind(this);
        this.send = this.send.bind(this);
        this.state = {value: 'rock'};
    }
    handleChange(event) {
        this.setState({value: event.target.value});
    }
    send(){
        var _this = this;
        _this.props.submitter(_this.state.value);
    }
    render(){
        return(
            <div>
                <h4>Pick</h4>
                <select onChange={this.handleChange}>
                    <option value="rock">Rock</option>
                    <option value="paper">Paper</option>
                    <option value="scissors">Scissors</option>
                </select>
                <button onClick={this.send}>Send</button>
            </div>
        );
    }
}

export default choosing;