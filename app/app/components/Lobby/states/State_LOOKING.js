import React from 'react'

class looking extends React.Component {
    constructor(){
        super();
        this.lookForOpponent = this.lookForOpponent.bind(this);
        this.state = {
            isLooking: false
        }
    }
    lookForOpponent(){
        this.setState({isLooking: true});
        this.props.seekOpponent();
    }
    render(){
        var _this = this;
        return(
            <div>
                <h1>Let's play Rock Paper Scissors!</h1>
                {(()=>{
                    if(_this.state.isLooking){
                        return <h3>Waiting for a opponent...</h3>
                    }else{
                        return <button onClick={_this.lookForOpponent}> Look for an opponent</button>;
                    }
                })()}
            </div>
        );
    }
}

export default looking;
