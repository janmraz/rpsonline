import React from 'react'

class revising extends React.Component {
    constructor(){
        super();
        this.reload = this.reload.bind(this);
    }
    reload(){
        location.reload();
    }
    render(){
        let _this = this;
        return(
            <div>
                <h6>Revising</h6>
                {(()=>{
                    if(_this.props.result == 'win'){
                        return <h3>You won</h3>;
                    }else if(_this.props.result == 'lose'){
                        return <h3>You Lost</h3>;
                    }else if(_this.props.result == 'tie'){
                        return <h3>It was a tie</h3>;
                    }else if(_this.props.result == 'none'){
                        return <h3>Problem</h3>;
                    }
                })()}
                <button onClick={_this.reload}>Let's go again!</button>
            </div>
        );
    }
}
export default revising;
