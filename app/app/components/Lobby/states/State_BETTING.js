import React from 'react'

class betting extends React.Component {
    constructor(){
        super();
        this.changedValue = this.changedValue.bind(this);
        this.state = {
            howMuch: ''
        }
    }
    changedValue(event){
        let number = event.target.value;
        if(number == '') this.setState({howMuch: ''});
        if(!isNaN(number)){
            if(number <= this.props.limit && number > -1){
                this.setState({howMuch: number});
            }
        }
    }
    render(){
        var _this = this;
        return(
            <div>
                <h1>Bet</h1>
                <div>Your account money: {_this.props.limit}</div>
                <input onChange={_this.changedValue} value={_this.state.howMuch}/>
                <button onClick={(()=>{_this.props.sendBet(_this.state.howMuch)})}>Bet</button>
            </div>
        );
    }
}

export default betting;