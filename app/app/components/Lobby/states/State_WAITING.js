import React from 'react'

class waiting extends React.Component {
    constructor(){
        super();
    }
    render(){
        return(
            <div>
                <h3>Waiting for a opponent to choose...</h3>
            </div>
        );
    }
}

export default waiting;