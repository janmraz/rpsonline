import React from 'react'
import { connect } from 'react-redux'

// components - states
import Choosing from './State_CHOOSING'
import Looking from './State_LOOKING'
import Revising from './State_REVISING'
import Waiting from './State_WAITING'
import Betting from './State_BETTING'
import BetWait from './State_BETWAIT'

class RootState extends React.Component {
    constructor(){
        super();
        this.submitSymbol = this.submitSymbol.bind(this);
        this.seekOpponent = this.seekOpponent.bind(this);
        this.sendBet = this.sendBet.bind(this);
        this.socket = io();
        let _this = this;
        this.socket.on('foundOpponent',function(data){
            if(data.id != _this.state.room){
                _this.setState({room: data.id},function () {
                    _this.props.NEXTState();
                    console.log('foundOpponent');
                });
            }
        });
        this.socket.on('findingOpponent',function(data){
            if(data.id != _this.state.room) {
                _this.setState({room: data.id});
                console.log('findingOpponent');
                _this.socket.on('foundOpponentForYou',function(data){
                    _this.props.NEXTState();
                    console.log('foundOpponentForYou');
                });
            }
        });

        this.socket.on('betDone',function(data){
            console.log('betDone',data);
            _this.props.NEXTState();
        });
        this.socket.on('result',function(data){
            console.log('result',data);
            let final_result;
            switch(data.result){
                case 'tie':
                    final_result = 'tie';
                    break;
                case 'win':
                    final_result = _this.props.user.user_id == data.gamer_one ? 'win' : 'lose';
                    break;
                case 'lose':
                    final_result = _this.props.user.user_id == data.gamer_one ? 'lose' : 'win';
                    break;
                default:
                    final_result = '';
            }
            _this.setState({result: final_result},function () {
                _this.props.NEXTState();
            });
        });
        this.state = {
            result: 'none',
            room: ''
        }
    }
    submitSymbol(symbol){
        let _this = this;
        _this.props.NEXTState();
        _this.socket.emit('sendingSymbol',{id: _this.state.room, gamer: _this.props.user.user_id,symbol: symbol});
    }
    seekOpponent(){
        this.socket.emit('lookForOpponent',this.props.user);
    }
    sendBet(number){
        let _this = this;
        if(!number) return;
        _this.socket.emit('betOn',{howMuch: number, id: _this.state.room, gamer: _this.props.user.user_id});
        _this.props.NEXTState();
    }
    render(){
        let _this = this;
        switch (_this.props.superstate){
            case 'revising':
                return <Revising result={_this.state.result} />;
            case 'waiting':
                return <Waiting />;
            case 'choosing':
                return <Choosing submitter={_this.submitSymbol}/>;
            case 'looking':
                return <Looking seekOpponent={_this.seekOpponent}/>;
            case 'betting':
                return <Betting sendBet={_this.sendBet} limit={_this.props.user.money} />;
            case 'betwait':
                return <BetWait />;
        }
    }
}
function mapStateToProps(state){
    console.log('state',state);
    return({
        user: state.user
    })
}
export default connect(mapStateToProps)(RootState);
