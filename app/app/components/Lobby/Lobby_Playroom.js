const React = require('react');
const {Link} = require('react-router');

// components
import RootState from './states/RootState';

// STATES ['looking','betting','betwait','revising','waiting','revising']

class Lobby_Playroom extends React.Component {
    constructor(){
        super();
        this.NEXTState = this.NEXTState.bind(this);
        this.state = {
            state: 'looking'
        }
    }
    NEXTState(){
        var _this = this;
        switch (this.state.state){
            case'looking':
                _this.setState({state: 'betting'});
                break;
            case'betting':
                _this.setState({state: 'betwait'});
                break;
            case'betwait':
                _this.setState({state: 'choosing'});
                break;
            case 'choosing':
                _this.setState({state: 'waiting'});
                break;
            case 'waiting':
                _this.setState({state: 'revising'});
                break;
            case'revising':
                _this.setState({state: 'looking'});
                break;
            default:
                console.error('what state is this ?',this.state.state);
        }
    }

    render() {
        return (
            <div className="container text-center">
                <RootState NEXTState={this.NEXTState} superstate={this.state.state} />
            </div>
        );
    }
}


export default Lobby_Playroom;
