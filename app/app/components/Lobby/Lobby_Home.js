const React = require('react');
const {Link} = require('react-router');

const Home = () => {
    return (
        <div className="container text-center">
            <h1>Welcome at Lobby!</h1>
            <Link to="/lobby/playroom" activeClassName="active">Let's play!</Link>
        </div>
    );
};


export default Home;