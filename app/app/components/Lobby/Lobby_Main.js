const React = require('react');

// components
import Lobby_Nav from './Nav';

const Lobby_Main = (props) => {
    return (
        <div>
            <Lobby_Nav />
            {props.children}
        </div>
    );
};


export default Lobby_Main;