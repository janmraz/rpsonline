const React = require('react');
const {Link} = require('react-router');

const Home = () => {
    return (
        <div className="container text-center">
            <h1>Welcome at RPS Online!</h1>
            <p>Enjoy game!</p>
            <Link to="/lobby" activeClassName="active">Let's go to the lobby!</Link>
        </div>
    );
};


export default Home;