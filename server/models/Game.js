var mongoose = require('mongoose');

module.exports.init = function () {
    var gameSchema = new mongoose.Schema({
        gamer_one: { type: String, default: ''},
        gamer_two: { type: String, default: ''},
        symbol_one: { type: String, default: ''},
        symbol_two: { type: String, default: ''},
        result: String,
        bet_one: { type: Number, default: 0},
        bet_two: { type: Number, default: 0},
        isSettled: { type: Boolean, default: false},
        isBet: { type: Boolean, default: false},
        isDone: { type: Boolean, default: false}
    });

    var Game = mongoose.model('Game', gameSchema);
};
