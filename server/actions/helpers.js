function isTie(first,second){
    return first === second;
}
function isWin(first,second) {
    switch(first){
        case 'rock':
            return second == 'scissors';
        case 'paper':
            return second == 'rock';
        case 'scissors':
            return second == 'paper';
        default:
            console.error('BAD FORMAT OF RPS SYMBOL');
    }
}

module.exports = {
    isWin: isWin,
    isTie: isTie
};